#Runs in O(n log n)

def counting_sort(A:list) -> list:
    B = [0 for _ in range(len(A)+1)]
    k = max(A)+1
    C = [0 for _ in range(k+1)]

    for j in range(0, len(A)):
        C[A[j]] = C[A[j]] + 1

    for i in range(0, k+1):
        C[i] += C[i-1]

    for j in range(0, len(A)):
        B[C[A[j]]] = A[j]
        C[A[j]]    -= 1
    
    return B
