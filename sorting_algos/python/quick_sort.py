#Runs in O(n^2)

def partition(A:list, l:int, r:int) -> None:
    pivot = A[r]
    i     = l - 1

    for j in range(l, r-1):
        if A[j] <= pivot:
            i += 1
            tmp_arr_i = A[i]
            A[i]      = A[j] 
            A[j]      = tmp_arr_i

    A[i+1], A[r]  = A[r], A[i+1]
    
    return (i+1)

def quick_sort(A:list, l:int, r:int) -> list:
    if len(A) == 1:
        return A
    
    if l < r:
        part = partition(A, l, r)
        quick_sort(A, l, part-1)
        quick_sort(A, part+1, r)
