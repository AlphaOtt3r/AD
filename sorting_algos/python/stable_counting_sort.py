from typing import List

def init_c(a: list, k: int, n: int) -> list:
    c = [0] * (k + 1)

    for j in range(0, n):
        c[a[j]] += 1

    for i in range(1, k + 1):
        c[i] += c[i - 1]

    return c


def counting_sort(a: list, b: list, k: int) -> list:
    n = len(a)
    c = init_c(a, k, n)

    for j in range(n - 1, -1, -1):
        b[c[a[j]] - 1] = a[j]
        c[a[j]] -= 1

    return b


def counting_sort_mod(a: list, b: list, k: int) -> list:
    n = len(a)
    c = init_c(a, k, n)

    for j in range(0, n):
        b[c[a[j]] - 1] = a[j]
        c[a[j]] -= 1

    return b


def counting_sort_init(a: List[int]) -> List[int]:
    b = [0] * len(a)
    k = max(a)
    return counting_sort(a, b, k)


if __name__ == '__main__':
    print(counting_sort_init([4, 2, 4, 1, 0, 3]))