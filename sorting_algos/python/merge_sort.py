#runs in O(|A| + |B|)

class merge_sort:
    def __init__(self, A:list) -> None:
        p = 1
        r = len(A)
        self.sort(A, p, r)

    def merge(self, A:list, p:int, q:int, r:int) -> None:
        la = A[p-1:q]
        ra = A[q:r]

        la.append(None)
        ra.append(None)

        i = j = 0
        for k in range(p-1, r):
            if la[i] is not None and (ra[j] is None or la[i] <= ra[j]):
                A[k] = la[i]
                i += 1
            else:
                A[k] = ra[j]
                j += 1

    def sort(self, A:list, p:int, r:int) -> None:
        if p < r:
            q = (p + r) // 2
            self.sort(A, p, q)
            self.sort(A, q+1, r)
            self.merge(A, p, q, r)
